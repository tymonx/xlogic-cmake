# Copyright 2020 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# More information:
# https://github.com/Kitware/CMake/blob/v3.18.4/Modules/CMakeAddNewLanguage.txt

set(CMAKE_SystemVerilog_OUTPUT_EXTENSION .json)
set(CMAKE_STATIC_LIBRARY_SUFFIX_SystemVerilog .archive.json)
set(CMAKE_SHARED_LIBRARY_SUFFIX_SystemVerilog .library.json)

set(CMAKE_INCLUDE_FLAG_SystemVerilog "-I ")
set(CMAKE_SystemVerilog_DEFINE_FLAG "-D ")
set(CMAKE_EXE_EXPORTS_SystemVerilog_FLAG "")

if (NOT CMAKE_SystemVerilog_COMPILE_OBJECT)
    set(CMAKE_SystemVerilog_COMPILE_OBJECT
        "<CMAKE_SystemVerilog_COMPILER> --mode=compile <DEFINES> <INCLUDES> <FLAGS> --output <OBJECT> <SOURCE>"
    )
endif()

# Create a static archive incrementally for large object file counts.
# If CMAKE_SystemVerilog_CREATE_STATIC_LIBRARY is set it will override these.
if (NOT DEFINED CMAKE_SystemVerilog_ARCHIVE_CREATE)
    set(CMAKE_SystemVerilog_ARCHIVE_CREATE
        "<CMAKE_SystemVerilog_COMPILER> --mode=archive --output <TARGET> <LINK_FLAGS> <OBJECTS>"
    )
endif()

if (NOT DEFINED CMAKE_SystemVerilog_ARCHIVE_APPEND)
    set(CMAKE_SystemVerilog_ARCHIVE_APPEND
        "<CMAKE_SystemVerilog_COMPILER> --mode=archive --append --output <TARGET> <LINK_FLAGS> <OBJECTS>"
    )
endif()

if (NOT DEFINED CMAKE_SystemVerilog_ARCHIVE_FINISH)
    set(CMAKE_SystemVerilog_ARCHIVE_FINISH
        "<CMAKE_SystemVerilog_COMPILER> --mode=archive --append --output <TARGET>"
    )
endif()

if(NOT CMAKE_SystemVerilog_CREATE_SHARED_LIBRARY)
    set(CMAKE_SystemVerilog_CREATE_SHARED_LIBRARY
        "<CMAKE_SystemVerilog_COMPILER> --mode=library <CMAKE_SHARED_LIBRARY_SystemVerilog_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_SystemVerilog_FLAGS> <SONAME_FLAG><TARGET_SONAME> --output <TARGET> <OBJECTS> <LINK_LIBRARIES>"
    )
endif()

if(NOT CMAKE_SystemVerilog_CREATE_SHARED_MODULE)
    set(CMAKE_SystemVerilog_CREATE_SHARED_MODULE
        ${CMAKE_SystemVerilog_CREATE_SHARED_LIBRARY}
    )
endif()

if(NOT CMAKE_SystemVerilog_LINK_EXECUTABLE)
    set(CMAKE_SystemVerilog_LINK_EXECUTABLE
        "<CMAKE_SystemVerilog_COMPILER> <FLAGS> <CMAKE_SystemVerilog_LINK_FLAGS> <LINK_FLAGS> --output <TARGET> <OBJECTS> <LINK_LIBRARIES>"
    )
endif()
